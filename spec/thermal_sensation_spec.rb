require 'thermal_sensation_proxy'
require_relative '../.config/configuration'

RSpec.describe 'La sensación térmica' do
  it 'es calculada a través del proxy' do
    temperature = 5
    wind = 23
    expected_temperature = 0.72

    thermal_proxy = ThermalSensationProxy.new(temperature, wind)

    expect(thermal_proxy.calcule).to eq(expected_temperature)
  end

  it 'es procesada por ThermalSensation sólo una vez si la petición se realiza en menos de un segundo' do
    temperature = 5
    wind = 23
    expected_temperature = 0.72

    thermal_proxy = ThermalSensationProxy.new(temperature, wind)

    expect_any_instance_of(ThermalSensation).to receive(:calcule).once.and_call_original
    thermal_proxy.calcule
    expect(thermal_proxy.calcule).to eq(expected_temperature)
  end

  it 'pasado un segundo vuelve a ser calculada por ThermalSensation' do
    expected_temperature = 0.72
    temperature = 5
    wind = 23

    thermal_proxy = ThermalSensationProxy.new(temperature, wind)

    expect_any_instance_of(ThermalSensation).to receive(:calcule).twice.and_call_original
    thermal_proxy.calcule
    sleep Configuration.wind_cache_seconds
    expect(thermal_proxy.calcule).to eq(expected_temperature)
  end
end
