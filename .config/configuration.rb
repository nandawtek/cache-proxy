require 'yaml'

class Configuration

  CACHE = YAML.load(File.open('./.config/cache.yml').read)

  class << self
    def wind_cache_seconds
      CACHE[environment]['seconds']['thermal_sensation']['wind']
    end

    private

    def environment
      return YAML.load(File.open('./.config/mode.yml').read)['environment'] if file?
      'production'
    end

    def file?
      File.file?('./.config/mode.yml')
    end
  end

end
