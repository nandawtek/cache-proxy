class Cache
  def initialize
    @cache = {}
  end

  def set(key, value)
    @cache[key] = value
  end

  def get(key)
    @cache[key]
  end

  def update_previous_call
    @previous_call = @actual_call
  end

  def update?(seconds)
    @actual_call = Time.now
    (@previous_call.nil? || expired_cache?(seconds))
  end

  private

  def expired_cache?(seconds)
    (@actual_call > @previous_call + seconds)
  end
end
