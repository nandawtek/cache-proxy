class ThermalSensation
  def initialize(temperature, wind)
    @temperature = temperature
    @wind = wind
  end
  def calcule
    (13.12 + (0.6215 * @temperature) -
    (11.37 * @wind ** 0.16) +
    (0.3965 * @temperature * @wind ** 0.16)).round(2)
  end
end
