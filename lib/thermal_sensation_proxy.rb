require 'thermal_sensation'
require 'cache'
require_relative '../.config/configuration'

class ThermalSensationProxy

  def initialize(temperature, wind)
    @cache = Cache.new
    @thermal_sensation = ThermalSensation.new(temperature, wind)
  end

  def calcule
    return @cache.get(:thermal_sensation) unless @cache.update?(Configuration.wind_cache_seconds)
    cache
  end

  private

  def cache
    @cache.update_previous_call
    @cache.set(:thermal_sensation, @thermal_sensation.calcule)
  end
end
